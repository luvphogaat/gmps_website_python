from django.contrib import admin
# from django.contrib.auth.models import Group

# Declaring models import
from .models import Contact, ResultPdf

class ContactAdmin(admin.ModelAdmin):
    # ...
    list_display = ('name', 'email', 'mob', 'message', 'date')
    list_filter = ('date',)

class ResultPdfAdmin(admin.ModelAdmin):
    list_display = ( 'linkname', 'link', 'date')


# Register your models here.

admin.site.register(Contact, ContactAdmin)
admin.site.register(ResultPdf, ResultPdfAdmin)


# admin.site.unregister(Group)